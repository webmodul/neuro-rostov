<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "lang://189">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

	<!-- СписокНовостейНаГлавной -->
<xsl:variable name="n" select="number(2)"/>

	<xsl:template match="/">

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">


  <div class="carousel-inner">
  <xsl:apply-templates select="/informationsystem"/>
  </div>
 <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
    <svg width="16" height="26" viewBox="0 0 16 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.5 3.5L12.5 0L0 12V13L12.5 26L15.5 23L5.5 13L15.5 3.5Z" fill="#3E8ECC"></path></svg>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
    <svg width="16" height="26" viewBox="0 0 16 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 3.5L3 0L15.5 12V13L3 26L0 23L10 13L0 3.5Z" fill="#3E8ECC"></path></svg>
  </button>
</div>


		
	</xsl:template>



	<xsl:template match="/informationsystem">
		<!-- Show informationsystem_item -->
		<xsl:if test="informationsystem_item">
		
				<xsl:apply-templates select="informationsystem_item[position() mod $n = 1]"/>
	
		</xsl:if>
	</xsl:template>

	<!-- informationsystem_item template -->
	<xsl:template match="informationsystem_item">
		 <div class="carousel-item">
		 <xsl:if test="position()=1">
			 <xsl:attribute name="class">carousel-item active</xsl:attribute>
			 </xsl:if>
		 <div class="row">
		<xsl:for-each select=". | following-sibling::informationsystem_item[position() &lt; $n]">
		<div class="col-md-6 mainpage-video-item">
			<xsl:value-of disable-output-escaping="yes" select="description" />
			<p class="title"><xsl:value-of select="name" /></p>
		</div>
		</xsl:for-each>
		</div>
</div>
	</xsl:template>


<xsl:template match="informationsystem_item" mode="indicators">
	<li data-target="#carouselExampleIndicators" data-slide-to="{position()-1}">
		<xsl:if test="position()=1">
			<xsl:attribute name="class">active</xsl:attribute>
		</xsl:if>
	</li>
</xsl:template>

</xsl:stylesheet>