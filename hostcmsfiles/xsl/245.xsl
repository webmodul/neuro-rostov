<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "lang://245">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<!-- СписокНовостейНаГлавной -->
	
	<xsl:template match="/">
		<xsl:apply-templates select="/informationsystem"/>
	</xsl:template>
	
	<xsl:template match="/informationsystem">
		<!-- Выводим название информационной системы -->

		
		<!-- Show informationsystem_item -->
		<xsl:if test="informationsystem_item">

				<xsl:apply-templates select="informationsystem_item"/>

		</xsl:if>

	</xsl:template>
	
	<!-- informationsystem_item template -->
	<xsl:template match="informationsystem_item">
		<!-- Text representation of a date -->
		
		   <section class="review">
                <i>
                    <xsl:value-of disable-output-escaping="yes" select="description"/>
                    <br/>
                    <p>
                        <xsl:value-of disable-output-escaping="yes" select="name"/><br/>
						<xsl:value-of select="date"/>
                    </p>
                </i>
            </section>
		
	</xsl:template>
</xsl:stylesheet>