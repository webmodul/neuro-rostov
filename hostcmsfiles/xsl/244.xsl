<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "lang://244">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<!-- СписокНовостейНаГлавной -->
	
	<xsl:template match="/">
	
		<xsl:apply-templates select="/informationsystem"/>
	
	</xsl:template>
	
	<xsl:template match="/informationsystem">

				<xsl:apply-templates select="informationsystem_item"/>
		
	</xsl:template>
	
	<!-- informationsystem_item template -->
	<xsl:template match="informationsystem_item">
				<div class="achievements__item">
				<div class="achievements__item-title">
			<xsl:value-of disable-output-escaping="yes" select="description"/>
		</div>

		<div class="achievements__item-text">
			<xsl:value-of disable-output-escaping="yes" select="text"/>
		</div>
		</div>
	</xsl:template>
</xsl:stylesheet>