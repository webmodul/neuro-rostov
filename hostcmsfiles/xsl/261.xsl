<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "lang://261">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<!-- СписокНовостейНаГлавной -->
	
	<xsl:template match="/">
		<xsl:apply-templates select="/informationsystem"/>
	</xsl:template>
	
	<xsl:template match="/informationsystem">
		<!-- Выводим название информационной системы -->
		
		
		<!-- Show informationsystem_item -->
		<xsl:if test="informationsystem_item">
			<ul>
				<xsl:apply-templates select="informationsystem_item"/>
			</ul>
			<ul style="list-style:none;">
			<li style="margin-top:15px;"><a href="/operaczii/slozhnye-sluchai/">Все операции</a></li>
			</ul>
		</xsl:if>
		
		
	</xsl:template>
	
	<!-- informationsystem_item template -->
	<xsl:template match="informationsystem_item">
		<!-- Text representation of a date -->
		<li><a href="{url}">
				<span class="articles-tabs__article-name">
					<xsl:value-of select="name"/>
				</span>
				<i class="articles-tabs__article-date">
					<!--<xsl:value-of select="date"/>-->
				</i>
			</a>
		</li>
		
	</xsl:template>
</xsl:stylesheet>