<?php

$Structure_Controller_Show = Core_Page::instance()->object;

$xslName = Core_Array::get(Core_Page::instance()->libParams, 'centralMenu');



$Structure_Controller_Show
	->xsl(
		Core_Entity::factory('Xsl')->getByName($xslName)
	)
	->parentId(CURRENT_STRUCTURE_ID)
	// Показывать группы информационных систем
	->showInformationsystemGroups(FALSE)
	// Показывать элементы информационных систем
	->showInformationsystemItems(FALSE)
	// Показывать группы магазина
	->showShopGroups(FALSE)
	// Показывать товары магазина
	->showShopItems(FALSE)
	->show();