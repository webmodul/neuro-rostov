<?php

require_once __DIR__ . '/mailer/Validator.php';
require_once __DIR__ . '/mailer/ContactMailer.php';

if (!Validator::isAjax() || !Validator::isPost()) {
	echo 'Доступ запрещен!';
	exit;
}

$name = isset($_POST['name']) ? trim(strip_tags($_POST['name'])) : null;
$fio = isset($_POST['fio']) ? trim(strip_tags($_POST['fio'])) : null;
$contacts = isset($_POST['contacts']) ? trim(strip_tags($_POST['contacts'])) : null;
$message = isset($_POST['message']) ? trim(strip_tags($_POST['message'])) : null;

if (empty($name) || empty($fio) || empty($contacts) || empty($message)) {
	echo 'Все поля обязательны для заполнения.';
	exit;
}

if (ContactMailer::send($name, $fio, $contacts, $message)) {
	echo htmlspecialchars($fio) . ', ваш запрос успешно отправлен 😊';
} else {
	echo 'Произошла ошибка! Не удалось отправить запрос 😞';
}
exit;
