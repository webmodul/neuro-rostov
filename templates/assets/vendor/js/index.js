jQuery(function () {
	jQuery(document).ready(function () {

		// Отправляет данные из формы на сервер и получает ответ
		jQuery('#feedbackForm').on('submit', function (event) {

			event.preventDefault();

			var form = $('#feedbackForm'),
				button = $('#feedbackFormButton'),
				answer = $('#answer'),
				loader = $('#loader');

			jQuery.ajax({
				url: 'vendor/handler.php',
				type: 'POST',
				data: form.serialize(),
				beforeSend: function () {
					answer.empty();
					button.attr('disabled', true);
					loader.fadeIn();
				},
				success: function (result) {
					loader.fadeOut(300, function () {
						answer.text(result);
					});
					form.find('.field').val('');
					button.attr('disabled', false);
				},
				error: function () {
					loader.fadeOut(300, function () {
						answer.text('Произошла ошибка! Попробуйте позже.');
					});
					button.attr('disabled', false);
				}
			});

		});

		var btn = $('#backToTop');

		$(window).scroll(function() {
		  if ($(window).scrollTop() > 300) {
			btn.addClass('show');
		  } else {
			btn.removeClass('show');
		  }
		});
		
		btn.on('click', function(e) {
		  e.preventDefault();
		  $('html, body').animate({scrollTop:0}, '300');
		});

		jQuery(function () {

			jQuery('.article-photo-slider').owlCarousel({
				items: 1,
				loop:true,
				nav:false,
				smartSpeed: 1500
			});

			jQuery('.reviews-slider').owlCarousel({
				items: 1,
				loop: true,
				dots: false,
				//autoplay: true,
				smartSpeed: 1500,
				responsive : {
					// breakpoint from 0 up
					0 : {
						nav: false,
						},
					576 : {
						nav: true,
						navText : ['<svg width="16" height="26" viewBox="0 0 16 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.5 3.5L12.5 0L0 12V13L12.5 26L15.5 23L5.5 13L15.5 3.5Z" fill="#3E8ECC"/><svg>','<svg width="16" height="26" viewBox="0 0 16 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 3.5L3 0L15.5 12V13L3 26L0 23L10 13L0 3.5Z" fill="#3E8ECC"/></svg>'],
					}
				}
			});

			jQuery('nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');

			$(function() {
				var owl = $('.gallery-slider'),

					owlOptions = {
					loop: false,
					margin: 0,
					smartSpeed: 700,
					nav: true,
					items: 1,
					dots: false,
					navText : ["<img src='img/mobile-slider-arrow-left.svg' alt=''>","<img src='img/mobile-slider-arrow-right.svg' alt=''>"]
					};

				if ( $(window).width() < 460 ) {
					var owlActive = owl.owlCarousel(owlOptions);
				} else {
					owl.addClass('off');
				}

				$(window).resize(function() {
					if ( $(window).width() < 460 ) {
						if ( $('.owl-carousel').hasClass('off') ) {
							var owlActive = owl.owlCarousel(owlOptions);
							owl.removeClass('off');
						}
					} else {
						if ( !$('.owl-carousel').hasClass('off') ) {
							owl.addClass('off').trigger('destroy.owl.carousel');
							owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
						}
					}
				});
			});

			jQuery('#loader2').css('display', 'none');
		});	

		var scroll = new SmoothScroll('a[href*="#"]');

		  $('.second-button').on('click', function () {
		
			$('.animated-icon2').toggleClass('open');
		  });


		if ($(window).width() < 767) {
			$('.collapse-div').each(function(){
			$(this).attr('data-toggle','collapse'); 
			})
		}

		$('.hiding-button').click(function(){
			$(this).fadeOut(1000)
		});

		var showChar = 670;
		var ellipsestext = "...";
		var moretext = "читать полностью";
		var lesstext = "скрыть все";
		$('.read-more').each(function() {
			var content = $(this).html();
	
			if(content.length > showChar) {
	
				var c = content.substr(0, showChar);
				var h = content.substr(showChar-1, content.length - showChar);
	
				var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<p class="text-sm-center review-show-full "><a href="" class="morelink">' + moretext + '</a></p></span>';
	
				$(this).html(html);
			}
	
		});
	
		$(".morelink").click(function(){
			if($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
			return false;
		});
	});
});